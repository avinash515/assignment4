#!/usr/bin/env python3

import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_to_db', methods=['POST'])
def add_to_db():
    print("Received request.")
    print(request.form['message'])
    msg = request.form['message']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
    cnx.commit()
    return hello()

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    check_cur = cnx.cursor(buffered=True)
    check_statement = "SELECT * FROM movies WHERE title = %s"
    check_val = (title, )
    try:
        check_cur.execute(check_statement, check_val)
        rows = check_cur.rowcount
        if (rows > 0):
            error_message = "Movie " + title + " already exists in database" 
            return render_template("index.html", message=error_message)
    except Exception as exp:
        our_message = "Movie " + title + " could not be inserted - " + str(exp)
        return render_template("index.html", message=our_message)

    try: 
        cur = cnx.cursor(buffered=True)
        statement = "INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)"
        values = (year, title, director, actor, release_date, rating)
        cur.execute(statement, values)
        cnx.commit()
        #check if this works
        success_message = "Movie " + title + " successfully inserted"
        return render_template("index.html", message=success_message)
    except Exception as exp:
        our_message = "Movie " + title + " could not be inserted - " + str(exp)
        return render_template("index.html", message=our_message)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    
    check_cur = cnx.cursor(buffered=True)
    check_statement = "SELECT * FROM movies WHERE title = %s"
    check_val = (title, )
    try:
        check_cur.execute(check_statement, check_val)
        rows = check_cur.rowcount
        if (rows == 0):
            error_message = "Movie " + title + " does not exist in database" 
            return render_template("index.html", message=error_message)
    except Exception as exp:
        our_message = "Movie " + title + " could not be updated - " + str(exp)
        return render_template("index.html", message=our_message)

    try: 
        cur = cnx.cursor()
        statement = "UPDATE movies SET year = %s, title = %s, director = %s, actor = %s, release_date = %s, rating = %s WHERE title = %s"
        values = (year, title, director, actor, release_date, rating, title)
        cur.execute(statement, values)
        cnx.commit()
        #check if this works
        success_message = "Movie " + title + " successfully updated"
        return render_template("index.html", message=success_message)
    except Exception as exp:
        our_message = "Movie " + title + " could not be updated - " + str(exp)
        return render_template("index.html", message=our_message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    check_cur = cnx.cursor(buffered=True)
    check_statement = "SELECT * FROM movies WHERE title = %s"
    check_val = (title, )
    try:
        check_cur.execute(check_statement, check_val)
        rows = check_cur.rowcount
        if (rows == 0):
            error_message = "Movie " + title + " does not exist" 
            return render_template("index.html", message=error_message)
    except Exception as exp:
        our_message = "Movie " + title + " could not be deleted - " + str(exp)
        return render_template("index.html", message=our_message)

    try: 
        cur = cnx.cursor()
        statement = "DELETE FROM movies WHERE title = %s"
        values = (title, )
        cur.execute(statement, values)
        cnx.commit()
        #check if this works
        success_message = "Movie " + title + " successfully deleted"
        return render_template("index.html", message=success_message)
    except Exception as exp:
        our_message = "Movie " + title + " could not be deleted - " + str(exp)
        return render_template("index.html", message=our_message)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    actor = request.args.get("search_actor")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    check_cur = cnx.cursor(buffered=True)
    check_statement = "SELECT * FROM movies WHERE actor = %s"
    check_val = (actor, )
    try:
        check_cur.execute(check_statement, check_val)
        rows = check_cur.rowcount
        if (rows == 0):
            error_message = "No movies found for actor " + actor 
            return render_template("index.html", message=error_message)
    except Exception as exp:
        return render_template("index.html", message=exp)

    try: 
        cur = cnx.cursor()
        statement = "SELECT * FROM movies WHERE actor = %s"
        values = (actor, )
        cur.execute(statement, values)
        my_list = list(cur.fetchall())
        title_list = [str(title) for my_id, year, title, director, actor, date, rating in my_list]
        year_list = [str(year) for my_id, year, title, director, actor, date, rating in my_list]
        actor_list = [str(actor) for my_id, year, title, director, actor, date, rating in my_list]
        final_list = zip(title_list, year_list, actor_list)
        return render_template("index.html", actorlist=final_list)
    except Exception as exp:
        return render_template("index.html", message=exp)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try: 
        cur = cnx.cursor()
        statement = "select title, year, actor, director, rating from movies where rating = (select max(rating)from movies)"
        cur.execute(statement)
        my_list = list(cur.fetchall())
        title_list = [str(title) for title, year, actor, director, rating in my_list]
        year_list = [str(year) for title, year, actor, director, rating in my_list]
        actor_list = [str(actor) for title, year, actor, director, rating in my_list]
        director_list = [str(director) for title, year, actor, director, rating in my_list]
        rating_list = [str(rating) for title, year, actor, director, rating in my_list]
        final_list = zip(title_list, year_list, actor_list, director_list, rating_list)
        return render_template("index.html", highlist=final_list)
    except Exception as exp:
        return render_template("index.html", message=exp)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try: 
        cur = cnx.cursor()
        statement = "select title, year, actor, director, rating from movies where rating = (select min(rating)from movies)"
        cur.execute(statement)
        my_list = list(cur.fetchall())
        title_list = [str(title) for title, year, actor, director, rating in my_list]
        year_list = [str(year) for title, year, actor, director, rating in my_list]
        actor_list = [str(actor) for title, year, actor, director, rating in my_list]
        director_list = [str(director) for title, year, actor, director, rating in my_list]
        rating_list = [str(rating) for title, year, actor, director, rating in my_list]
        final_list = zip(title_list, year_list, actor_list, director_list, rating_list)
        return render_template("index.html", lowlist=final_list)
    except Exception as exp:
        return render_template("index.html", message=exp)
 



@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
